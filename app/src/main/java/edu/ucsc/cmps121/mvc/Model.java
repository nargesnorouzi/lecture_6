package edu.ucsc.cmps121.mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Created by nargesnorouzi on 2017-10-17.
 */

public class Model extends Observable{

    private List<Integer> list;
    public Model(){
        list = new ArrayList<Integer>(3);
        list.add(0);
        list.add(0);
        list.add(0);
    }
    public int getValueAtIndex(int index){

        return list.get(index);
    }
    public void setValueAtIndex(int index){
        list.set(index, list.get(index) + 1);
        setChanged();
        notifyObservers();
    }

}
