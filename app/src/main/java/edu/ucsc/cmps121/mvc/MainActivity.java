package edu.ucsc.cmps121.mvc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer, Button.OnClickListener{
    private Model model;
    private Button button1;
    private Button button2;
    private Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        button1 = (Button)findViewById(R.id.button);
        button2 = (Button)findViewById(R.id.button2);
        button3 = (Button)findViewById(R.id.button3);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        model = new Model();
        model.addObserver(this);


    }

    @Override
    public void update(Observable observable, Object o) {
        ((TextView)findViewById(R.id.textView)).setText(String.valueOf(model.getValueAtIndex(0)));
        ((TextView)findViewById(R.id.textView2)).setText(String.valueOf(model.getValueAtIndex(1)));
        ((TextView)findViewById(R.id.textView3)).setText(String.valueOf(model.getValueAtIndex(2)));
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                model.setValueAtIndex(0);
                break;
            case R.id.button2:
                model.setValueAtIndex(1);
                break;
            case R.id.button3:
                model.setValueAtIndex(2);
                break;
        }

    }
}
